package com.online_shop.dao;

public interface ProductDao {
    int updatePrice(Double newPrice, Long id);
}
