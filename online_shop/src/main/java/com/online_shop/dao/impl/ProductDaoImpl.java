package com.online_shop.dao.impl;

import com.online_shop.dao.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDaoImpl implements ProductDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int updatePrice(Double newPrice, Long id) {
        return jdbcTemplate.update("UPDATE products SET product_price_is_now = ? WHERE id = ?", new Object[]{newPrice, id});
    }
}
