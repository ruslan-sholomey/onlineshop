package com.online_shop.dao.impl;

import com.online_shop.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int activateUser(String key) {
        return jdbcTemplate.update("UPDATE users SET activated = 1 WHERE activation_key = ?", new Object[]{key});
    }
}
