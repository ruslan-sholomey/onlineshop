package com.online_shop.service;

import com.online_shop.model.Order;
import com.online_shop.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderService {
    Order saveOrder(Order order);
    Iterable<Order> getAllOrderByUserId(Long id);
    Order getOrderById(Long id);
    Page<Order> getAllOrderByUser(Pageable pageable, User user);
}
