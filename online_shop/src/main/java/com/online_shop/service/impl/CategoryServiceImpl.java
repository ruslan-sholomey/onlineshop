package com.online_shop.service.impl;

import com.online_shop.model.Category;
import com.online_shop.repository.CategoryRepository;
import com.online_shop.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Iterable<Category> getAllCategory() {
        return categoryRepository.findAll();
    }
}
