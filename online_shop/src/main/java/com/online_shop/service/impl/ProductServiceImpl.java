package com.online_shop.service.impl;

import com.online_shop.model.User;
import com.online_shop.repository.ProductRepository;
import com.online_shop.model.Product;
import com.online_shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product getProductById(Long id) {
        return productRepository.findOne(id);
    }

    @Override
    public List<Product> getAllProduct(Long userId) {
        return productRepository.getAllProductByUserId(userId);
    }

    @Override
    public List<Product> listAllProducts() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    public Page<Product> getAllProduct(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    @Override
    public Page<Product> getAllProductByCategoryId(Pageable pageable, Long id) {
        return productRepository.findProductByCategoryId(pageable, id);
    }

    @Override
    public Page<Product> getAllProductByUser(Pageable pageable, User user) {
        return productRepository.findProductByUser(pageable, user);
    }

    @Override
    public int updateProduct(Double price, Long id) {
        return 0;
    }
}
