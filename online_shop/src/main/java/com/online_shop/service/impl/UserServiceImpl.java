package com.online_shop.service.impl;

import com.online_shop.repository.UserRepository;
import com.online_shop.dao.UserDao;
import com.online_shop.model.Authority;
import com.online_shop.model.User;
import com.online_shop.service.MailService;
import com.online_shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private MailService mailService;

    @Autowired
    private UserDao userDao;

    @Override
    public User registerNewUser(User user, String baseUrl) {
        String activationKey = UUID.randomUUID().toString();
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        user.setRole(Authority.ROLE_USER);
        user.setActivationKey(activationKey);
        User savedUser = userRepository.save(user);
        if (!Objects.isNull(savedUser)){
            mailService.sendActivationEmail(savedUser.getEmail(), savedUser.getActivationKey(), savedUser, baseUrl);
        }
        return savedUser;
    }
    @Override
    public boolean isUserExists(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public boolean activateAccount(String activationKey) {
        int countUpdates = userDao.activateUser(activationKey);
        if(countUpdates != 0)
            return true;
        return false;
    }

    @Override
    public boolean isAccountActivated(String activationKey) {
        return userRepository.isAccountActivated(activationKey);
    }

    @Override
    public List<User> getAll() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public User getById(Long userId) {
        return userRepository.findOne(userId);
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }
}
