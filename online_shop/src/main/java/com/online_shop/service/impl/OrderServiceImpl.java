package com.online_shop.service.impl;

import com.online_shop.model.Order;
import com.online_shop.model.User;
import com.online_shop.repository.OrderRepository;
import com.online_shop.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public Order saveOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public Iterable<Order> getAllOrderByUserId(Long id) {
        return orderRepository.getAllOrderByUserId(id);
    }

    @Override
    public Order getOrderById(Long id) {
        return orderRepository.findOne(id);
    }

    @Override
    public Page<Order> getAllOrderByUser(Pageable pageable, User user) {
        return orderRepository.findOrdertByUser(pageable, user);
    }
}
