package com.online_shop.service;

import com.online_shop.model.Category;

public interface CategoryService {
    Iterable<Category> getAllCategory();

}
