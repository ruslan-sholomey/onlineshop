package com.online_shop.service;

import com.online_shop.model.User;

import java.util.List;

public interface UserService {
    User registerNewUser(User user, String baseUrl);
    boolean isUserExists(String email);
    boolean activateAccount(String activationKey);
    boolean isAccountActivated(String activationKey);
    List<User> getAll();
    User getById(Long userId);
    User getUserByEmail(String email);
}
