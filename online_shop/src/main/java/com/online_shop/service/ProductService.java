package com.online_shop.service;

import com.online_shop.model.Product;
import com.online_shop.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {
    Product saveProduct(Product product);
    Product getProductById(Long id);
    List<Product> getAllProduct(Long userId);
    List<Product> listAllProducts();
    Page<Product> getAllProduct(Pageable pageable);
    Page<Product> getAllProductByCategoryId(Pageable pageable, Long id);
    Page<Product> getAllProductByUser(Pageable pageable, User user);
    int updateProduct(Double price, Long id);
}

