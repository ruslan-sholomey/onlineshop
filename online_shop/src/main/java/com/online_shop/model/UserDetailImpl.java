package com.online_shop.model;

import org.springframework.security.core.authority.AuthorityUtils;

public class UserDetailImpl extends org.springframework.security.core.userdetails.User {

    private User user;

    public UserDetailImpl(User user) {
        super(user.getEmail(), user.getPassword(), user.isActivated(), true, true, true,  AuthorityUtils.createAuthorityList(user.getRole().toString()));
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public Long getId() {
        return user.getId();
    }

    public Authority getRole() {
        return user.getRole();
    }
}
