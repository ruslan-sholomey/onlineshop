package com.online_shop.model;

public enum Authority {

    ROLE_ADMIN,
    ROLE_USER
}
