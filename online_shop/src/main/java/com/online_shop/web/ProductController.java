package com.online_shop.web;

import com.online_shop.model.Order;
import com.online_shop.model.Product;
import com.online_shop.model.User;
import com.online_shop.model.UserDetailImpl;
import com.online_shop.scheduler.UpdatePrice;
import com.online_shop.service.CategoryService;
import com.online_shop.service.OrderService;
import com.online_shop.service.ProductService;
import com.online_shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class ProductController {

    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private UserService userService;
    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/shop", method = RequestMethod.GET)
    public String list(Model model,  Pageable pageable){
        Page<Product> productPage = productService.getAllProduct(pageable);
        PageWrapper<Product> page = new PageWrapper<Product>(productPage, "/shop");
        model.addAttribute("products", page.getContent());
        model.addAttribute("page", page);
        model.addAttribute("categories", categoryService.getAllCategory());
        return "shop";
    }

    @RequestMapping(value = "/shop/{id}", method = RequestMethod.GET)
    public String listByCategory(@PathVariable("id") Long id, Model model,  Pageable pageable){
        Page<Product> productPage = productService.getAllProductByCategoryId(pageable, id);
        PageWrapper<Product> page = new PageWrapper<Product>(productPage, "/shop");
        model.addAttribute("products", page.getContent());
        model.addAttribute("page", page);
        model.addAttribute("categories", categoryService.getAllCategory());
        return "shop";
    }

    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public String handleFileUpload(Model model){
        model.addAttribute("product", new Product());
        model.addAttribute("allCategory", categoryService.getAllCategory());
        return "product";
    }

    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public String handleFileUpload(@RequestParam("product") MultipartFile fileUpload,
                                   @ModelAttribute("product") Product product) throws Exception {

        Product product1 = new Product();
        product1.setProductName(product.getProductName());
        product1.setImage(fileUpload.getBytes());
        product1.setPrice(product.getPrice());
        product1.setPriceIsNow(product.getPrice());
        product1.setDescription(product.getDescription());
        product1.setCategoryId(product.getCategoryId());
        UserDetailImpl userDetail = (UserDetailImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.getUserByEmail(userDetail.getUsername());
        product1.setUser(user);
        productService.saveProduct(product1);

        return "redirect:/home";
    }

    @RequestMapping(value = "/imageDisplay/{id}", method = RequestMethod.GET)
    public ResponseEntity<byte []> displayImage(@PathVariable("id") Long id) {

        byte[] imageContent = productService.getProductById(id).getImage();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        return new ResponseEntity<byte[]>(imageContent, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(Model model,  Pageable pageable){
        UserDetailImpl userDetail = (UserDetailImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.getUserByEmail(userDetail.getUsername());
        Page<Product> productPage = productService.getAllProductByUser(pageable, user);
        PageWrapper<Product> page = new PageWrapper<Product>(productPage, "/home");
        model.addAttribute("products", page.getContent());
        model.addAttribute("page", page);
        return "home";
    }

    @RequestMapping(value = "/home/order", method = RequestMethod.GET)
    public String orderList(Model model, Pageable pageable){
        UserDetailImpl userDetail = (UserDetailImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.getUserByEmail(userDetail.getUsername());
        Page<Order> orderPage = orderService.getAllOrderByUser(pageable, user);
        PageWrapper<Order> page = new PageWrapper<Order>(orderPage, "/home/order");
        model.addAttribute("orders", page.getContent());
        model.addAttribute("page", page);
        return "orderList";
    }

    @RequestMapping(value = "home/order/details/{id}", method = RequestMethod.GET)
    public String orderDetails(@PathVariable("id") Long id, Model model){
        model.addAttribute("order", orderService.getOrderById(id));
        return "orderDetails";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(){
        return "login";
    }

    @RequestMapping(value = "product/details/{id}", method = RequestMethod.GET)
    public String productDetails(@PathVariable("id") Long id, Model model){
        model.addAttribute("product", productService.getProductById(id));
        model.addAttribute("categories", categoryService.getAllCategory());
        return "productDetails";
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.GET)
    public String order(Model model, @PathVariable("id") Long id){
        model.addAttribute("order", new Order());
        return "orderForm";
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.POST)
    public String order(@ModelAttribute("order") Order order, @PathVariable("id") Long id){
        Order newOrder = new Order();
        newOrder.setFirstName(order.getFirstName());
        newOrder.setLastName(order.getLastName());
        newOrder.setPhoneNumber(order.getPhoneNumber());
        newOrder.setCity(order.getCity());
        newOrder.setAddress(order.getAddress());
        Product product = productService.getProductById(id);
        newOrder.setProduct(product);
        newOrder.setUser(product.getUser());
        orderService.saveOrder(newOrder);
        return "redirect:/shop";
    }
}
