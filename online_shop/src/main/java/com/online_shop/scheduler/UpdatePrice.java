package com.online_shop.scheduler;

import com.online_shop.dao.ProductDao;
import com.online_shop.model.Product;
import com.online_shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Component
public class UpdatePrice {

    @Autowired
    private ProductDao productDao;

    @Autowired
    private ProductService productService;

    @Scheduled(cron = "0 01 00 * * ?")
    public void scheduledUpdatePrice(){
        List<Product> productList = productService.listAllProducts();
        for (Product product : productList) {
            if (product.getPriceIsNow() > 0){
                double priceAfterRateCalculation = new BigDecimal(product.getPriceIsNow() -
                        (product.getPriceIsNow() * 0.01)).setScale(2, RoundingMode.UP).doubleValue();
                productDao.updatePrice(priceAfterRateCalculation, product.getId());
            }
        }
    }
}
