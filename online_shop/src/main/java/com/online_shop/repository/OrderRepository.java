package com.online_shop.repository;

import com.online_shop.model.Order;
import com.online_shop.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Long> {
    @Query("SELECT u from Order u where u.user.id = :id")
    List<Order> getAllOrderByUserId(@Param("id") Long id);

    Page<Order> findOrdertByUser(Pageable pageable, User user);
}
