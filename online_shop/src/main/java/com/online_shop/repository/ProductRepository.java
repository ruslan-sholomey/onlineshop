package com.online_shop.repository;

import com.online_shop.model.Product;
import com.online_shop.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Long>{
    @Query("SELECT u from Product u where u.user.id = :id")
    List<Product> getAllProductByUserId(@Param("id") Long id);

    Page<Product> findProductByCategoryId(Pageable pageable, Long categoryId);

    Page<Product> findProductByUser(Pageable pageable, User user);

    Page<Product> findAll(Pageable pageable);

}
